/*
Copyright 2021 Bertrand Madet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

"use strict";

class Position {
  constructor(name, data) {
    this.name = name;
    this.cost = data.cost;
    this.minimum = data.minimum ?? 0;
    this.maximum = data.maximum ?? 16;
    this.color = data.color;
    this.movementAllowance = data.movementAllowance ?? '-';
    this.strength = data.strength ?? '-';
    this.agility = data.agility ?? '-';
    this.passingAbility = data.passingAbility ?? '-';
    this.armorValue = data.armorValue ?? '-';
    this.skills = data.skills ? [...data.skills] : [];
    this.traits = data.traits ? [...data.traits] : [];
  }
}

class Roster {
  constructor(name, data) {
    this.name = name;
    this.reroll = data.reroll;
    this.apothecary = data.apothecary;
    this.tier = data.tier;

    this.positions = new Map();
    for (const key in data.positions) {
      this.positions.set(key, new Position(key, data.positions[key]));
    };
    this.positionRestriction = {
      max: data.positionRestriction ? data.positionRestriction.max : 1,
      of: data.positionRestriction ? [...data.positionRestriction.of] : [],
    };
  }
}

class Rules {
  constructor(data) {
    this.modes = new Map();
    for (let name in data.modes) {
      this.modes.set(name, { name, treasury: data.modes[name] });
    };
    this.rosters = new Map();
    for (let name in data.rosters) {
      this.rosters.set(name, new Roster(name, data.rosters[name]));
    };
  }

  modeNames() {
    return [...this.modes.keys()];
  }

  rosterNames() {
    return [...this.rosters.keys()];
  }
}

class Team {
  constructor(roster, budget = 1000) {
    this.roster = roster;
    this.name = '';
    this.rerollCount = 0;
    this.players = [];
    this.apothecary = false;
    this.budget = budget;
  }

  addReroll() {
    this.rerollCount += 1;
  }

  removeReroll() {
    if (this.rerollCount > 0) {
      this.rerollCount -= 1;
    }
  }

  addPlayer(positionName) {
    const number = this.players.reduce((maxNumber, player) => {
      if (player.number >= maxNumber) {
        maxNumber = player.number + 1;
      }
      return maxNumber
    }, 1);
    this.players.push({
      number,
      name: `${positionName} ${number}`,
      position: this.roster.positions.get(positionName)
    })
  }

  removePlayer(index) {
    this.players.splice(index, 1);
  }

  rerollValue() {
    return this.rerollCount * (this.roster !== null ? this.roster.reroll : 0);
  }

  value() {
    const playersValue = this.players.reduce((acc, current) => {
      return acc + current.position.cost;
    }, 0);
    const apothecaryValue = this.apothecary ? 50 : 0;
    return this.rerollValue() + playersValue + apothecaryValue;
  }

  getAvailableForPosition(countByPosition, hasAPositionRestriction) {
    return (name) => {
      let available = (countByPosition.get(name) || 0) < this.roster.positions.get(name).maximum;
      if (available && hasAPositionRestriction) {
        available = this.roster.positionRestriction.of.indexOf(name) === -1
      }
      return {
        position: name,
        available
      };
    }
  }

  getAvailablePositions() {
    const countByPosition = this.countByPosition();
    const hasAPositionRestriction = this.hasPositionRestriction(countByPosition);
    return [...this.roster.positions.keys()].map(this.getAvailableForPosition(countByPosition, hasAPositionRestriction), this);
  }

  availableApothecary() {
    return !this.apothecary;
  }

  countByPosition() {
    return this.players.reduce((countByPosition, player) => {
      let count = 1;
      if (countByPosition.has(player.position.name)) {
        count = countByPosition.get(player.position.name);
        count += 1;
      }
      countByPosition.set(player.position.name, count);
      return countByPosition;
    }, new Map());
  }

  hasPositionRestriction(countByPosition) {
    let hasAPositionRestriction = false;
    if (this.roster.positionRestriction && this.roster.positionRestriction.of.length) {
      const number = this.roster.positionRestriction.of.reduce((sum, position) => {
        return sum + (countByPosition.has(position) ? countByPosition.get(position) : 0);
      }, 0);
      hasAPositionRestriction = number >= this.roster.positionRestriction.max;
    }
    return hasAPositionRestriction;
  }
}