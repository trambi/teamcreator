/*
Copyright 2021 Bertrand Madet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

function init() {
  window.controller = new Controller(new View());
  fetch('rules.json').then(response => {
    if (response.status === 200) {
      return response.json();
    }
  }).then((data) => {
    controller.loadData(data);
  }).catch((err) => {
    console.error(err)
  });
}

class Controller {
  constructor(view) {
    this.view = view;
    this.status = 'waiting';
    this.rules = null;
    this.team = null;

  }

  loadData(data) {
    this.rules = new Rules(data);
    this.treasury = this.rules.modes.get('league').treasury;
    this.status = 'ready';
    this.view.setRosters(this.rules.rosterNames());
    this.updateTreasury();
  }

  selectRoster(value) {
    var currentRoster = this.rules.rosters.get(value);
    this.team = new Team(currentRoster);
    this.view.setRosterElements(currentRoster, this);
    this.view.updatePlayers(this.team.players, this);
    this.view.updatePlayerCount(this.team.players.length);
    this.view.updateRerollSummary(`${this.team.rerollCount} (${this.team.rerollValue()}k)`);
    this.view.setRosterSummary(currentRoster)
    this.updateTreasury();
  }

  handleEvent(event) {
    switch (event.type) {
      case 'click': {
        this.handleClickEvent(event);
        this.updateTreasury();
        break;
      }
      case 'input': {
        this.handleInputEvent(event);
        break;
      }
      case 'blur': {
        this.handleBlurEvent(event);
        break;
      }
      default: {
        console.log('unknowType :' + event.type);
      }
    }
  }

  handleClickEvent(event) {
    if (event.target.id === 'roster_reroll') {
      this.team.addReroll();
      this.view.updateRerollSummary(`${this.team.rerollCount} (${this.team.rerollValue()}k)`, this);
    } else if (event.target.id === 'roster_apothecary') {
      this.team.apothecary = true;
      this.view.updateApothecarySummary(`${this.team.apothecary ? 1 : 0} (50k)`, this);
    } else if (event.target.id.startsWith('roster_position')) {
      this.team.addPlayer(event.target.id.replace('roster_position_', ''));
      this.view.updatePlayers(this.team.players, this);
      this.view.updatePlayerCount(this.team.players.length);
      this.view.updateRosterElements(this.team.getAvailablePositions(), this.team.availableApothecary());
    } else if (event.target.id.startsWith('team_player_delete_')) {
      const index = parseInt(event.target.id.replace('team_player_delete_', ''), 10);
      this.team.removePlayer(index);
      this.view.updatePlayers(this.team.players, this);
      this.view.updatePlayerCount(this.team.players.length);
      this.view.updateRosterElements(this.team.getAvailablePositions(), this.team.availableApothecary());
    } else if (event.target.id === 'team_reroll_delete') {
      this.team.removeReroll();
      this.view.updateRerollSummary(`${this.team.rerollCount} (${this.team.rerollValue()}k)`, this);
    } else if (event.target.id === 'team_apothecary_delete') {
      this.team.apothecary = false;
      this.view.updateApothecarySummary(`${this.team.apothecary ? 1 : 0} (50k)`, this);
    }
  }

  handleInputEvent(event) {
    if (event.target.id.startsWith('team_player_number_')) {
      const index = parseInt(event.target.id.replace('team_player_number_', ''), 10);
      this.team.players[index].number = parseInt(event.target.value, 10);
    }
  }

  handleBlurEvent(event) {
    if (event.target.id.startsWith('team_player_name_')) {
      const index = parseInt(event.target.id.replace('team_player_name_', ''), 10);
      this.team.players[index].name = event.target.value;
    }
  }

  setBudget(budget){
    this.treasury = parseInt(budget,10);
    updateTreasury();
  }

  updateTreasury() {
    const value = this.team !== null ? this.team.value() : 0;
    this.view.updateTreasurySummary(value, this.treasury, this.treasury - value);
  }

  exportTeamToClipboard(){
    this.team.name = document.getElementById('name').value;
    const textToExport = exportTeamToText(this.team);
    navigator.clipboard.writeText(textToExport).then(() => {
      alert('Team as text copied in clipboard');
    }, () => {
      alert('Error while writting in clipboard');
    });
  }

  switchOptions() {
    const optionsElt = document.getElementById('options');
    optionsElt.style.display = optionsElt.style.display === 'none' ? 'block' : 'none';
  }
}