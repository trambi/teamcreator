/*
Copyright 2021 Bertrand Madet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

QUnit.module('exportTeamToText', () => {

  QUnit.test('should text summary of team', (assert) => {
    const team = new Team(new Roster('Human', {
      reroll: 50,
      apothecary: true,
      tier: 1,
      positions: {
        Thrower: {
          cost: 80,
          minimum: 0,
          maximum: 2,
          color: "white",
          movementAllowance: 6,
          strength: 3,
          agility: "3+",
          passingAbility: "2+",
          armorValue: "9+",
          skills: ["Pass", "Sure Hands"],
          traits: []
        },
        Blitzer: {
          cost: 85,
          minimum: 0,
          maximum: 4,
          color: "red",
          movementAllowance: 7,
          strength: 3,
          agility: "3+",
          passingAbility: "4+",
          armorValue: "9+",
          skills: ["Block"],
          traits: []
        }
      }
    }));
    team.addPlayer('Blitzer');
    team.addPlayer('Thrower');
    team.addPlayer('Thrower');
    team.removePlayer(1);
    team.addReroll();
    team.addReroll();
    team.apothecary = true;
    team.name = 'TeamName'
    const expected = `TeamName - Human

1. Blitzer 1 - Blitzer
3. Thrower 3 - Thrower

Rerolls: 2
Apothecary: 1
Treasury: 685 000
Value: 315 000`;
    assert.equal(exportTeamToText(team), expected);
  });

});
