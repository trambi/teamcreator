/*
Copyright 2021 Bertrand Madet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

class View {
  constructor() {

  }
  setRosters(rosterNames) {
    const select = document.getElementById('roster');
    const defaultOption = document.createElement('option');
    defaultOption.id = '';
    defaultOption.innerHTML = '--';
    defaultOption.selected = 'selected';
    select.appendChild(defaultOption);
    rosterNames.forEach((name) => {
      const option = document.createElement('option');
      option.id = name;
      option.innerHTML = name;
      select.appendChild(option);
    });
  }

  createRosterElement(id, text) {
    const elt = document.createElement('button');
    elt.id = id;
    elt.type = 'button';
    elt.innerText = text;
    return elt;
  }

  createPositionRow(position) {
    return `<tr>
    <td>${position.minimum}-${position.maximum}</td>
    <td>${position.name}</td>
    <td>${position.cost} k</td>
    <td>${position.movementAllowance}</td>
    <td>${position.strength}</td>
    <td>${position.agility}</td>
    <td>${position.passingAbility}</td>
    <td>${position.armorValue}</td>
    <td>${position.skills.concat(position.traits).sort().join(', ')}</td>
</tr>`;
  }

  createRosterTable(roster) {
    let table = [...roster.positions.values()].filter((position) => {
      if (roster.positionRestriction && -1 !== roster.positionRestriction.of.indexOf(position.name)) {
        return false;
      }
      return true;
    }).reduce((acc, position) => {
      return acc + this.createPositionRow(position);
    }, '');
    if (roster.positionRestriction.of.length) {
      const max = roster.positionRestriction.max;
      const phrase = `A ${roster.name} team may include up to ${max} big guy${max > 1 ? 's' : ''} from among the following`;
      table += `<tr><td colspan="9">${phrase}</td>
</tr>`;
      table += roster.positionRestriction.of.reduce((acc, name) => {
        return acc + this.createPositionRow(roster.positions.get(name));
      }, '');
    }
    return table;
  }

  setRosterSummary(roster) {
    const positionsElt = document.getElementById('positions');
    while (positionsElt.firstChild) {
      positionsElt.removeChild(positionsElt.firstChild);
    }
    const summary = document.getElementById('roster_summary');
    if (roster === undefined) {
      summary.innerText = '--';
      return;
    }
    summary.innerText = roster.name + ' roster';
    positionsElt.innerHTML = this.createRosterTable(roster);
  }

  setRosterElements(roster, handler) {
    this.cleanRosterElements();
    if (roster === undefined) {
      return;
    }
    this.addPositionButtonsToRosterElements(roster, handler);
    this.addRerollButtonToRosterElements(roster, handler);
    this.addApothecaryButtonToRosterElements(roster, handler);
  }

  cleanRosterElements() {
    const buy = document.getElementById('buy');
    while (buy.firstChild) {
      buy.removeChild(buy.firstChild);
    }
  }

  addPositionButtonsToRosterElements(roster, handler) {
    [...roster.positions.values()].forEach((position) => {
      const positionElt = this.createRosterElement(`roster_position_${position.name}`, `${position.name} (${position.cost}k)`);
      positionElt.style.backgroundColor = position.color;
      positionElt.addEventListener('click', handler);
      document.getElementById('buy').appendChild(positionElt);
    });
  }

  addRerollButtonToRosterElements(roster, handler) {
    const reroll = this.createRosterElement('roster_reroll', `reroll (${roster.reroll}k)`);
    reroll.addEventListener('click', handler);
    document.getElementById('buy').appendChild(reroll);
  }

  addApothecaryButtonToRosterElements(roster, handler) {
    if (roster.apothecary) {
      const apo = this.createRosterElement('roster_apothecary', `apothecary (50k)`);
      apo.addEventListener('click', handler);
      document.getElementById('buy').appendChild(apo);
    }
  }

  updateRosterElements(availablePositions, availableApothecary) {
    const apothecaryElt = document.getElementById('roster_apothecary');
    if(apothecaryElt){
      apothecaryElt.disabled = !availableApothecary;
    }
    availablePositions.forEach(({ position, available }) => {
      const positionElt = document.getElementById(`roster_position_${position}`);
      positionElt.disabled = !available;
    });
  }

  updateTreasurySummary(value, treasury, remaining) {
    const details = document.getElementById('treasury');
    while (details.firstChild) {
      details.removeChild(details.firstChild);
    }
    const summary = document.createElement('summary');
    summary.innerText = `${value}/${treasury}`;
    const text = document.createTextNode(`Initial treasury: ${treasury}, remaining: ${remaining}`);
    details.appendChild(summary);
    details.appendChild(text);
  }

  updateRerollSummary(value, handler) {
    document.getElementById('team_reroll').innerText = value;
    const button = document.createElement('button');
    button.type = 'button';
    button.id = `team_reroll_delete`;
    button.innerText = '🗑️';
    button.addEventListener('click', handler);
    document.getElementById('team_reroll').appendChild(button);
  }

  updateApothecarySummary(value, handler) {
    document.getElementById('team_apothecary').innerText = value;
    const button = document.createElement('button');
    button.type = 'button';
    button.id = `team_apothecary_delete`;
    button.innerText = '🗑️';
    button.addEventListener('click', handler);
    document.getElementById('team_apothecary').appendChild(button);
  }

  createPlayerInteractCell(index, prefix, handler) {
    const cell = document.createElement('td');
    const button = document.createElement('button');
    button.type = 'button';
    button.id = `team_player_delete_${index}_${prefix}`;
    button.innerText = '🗑️';
    button.addEventListener('click', handler);
    cell.appendChild(button);
    return cell;
  }

  createPlayerNameCell(player, index, handler) {
    const cell = document.createElement('td');
    const name = document.createElement('input');
    name.type = 'text';
    name.id = `team_player_name_${index}`;
    name.value = player.name;
    name.addEventListener('blur', handler);
    cell.appendChild(name);
    return cell;
  }

  createPlayerNumberCell(player, index, handler) {
    const cell = document.createElement('td');
    const number = document.createElement('input');
    number.type = 'number';
    number.id = `team_player_number_${index}`;
    number.value = player.number;
    number.min = 0;
    number.addEventListener('input', handler);
    cell.appendChild(number);
    return cell;
  }

  createPlayerCostCell(player) {
    const cell = document.createElement('td');
    cell.innerText = `${player.position.cost} k`;
    return cell;
  }

  createPlayerPositionCell(player) {
    const cell = document.createElement('td');
    const position = document.createElement('span');
    position.innerText = player.position.name;
    position.style.backgroundColor = player.position.color;
    cell.appendChild(position);
    return cell
  }

  createPlayerLine(player, index, handler) {
    const line = document.createElement('tr');
    line.appendChild(this.createPlayerInteractCell(index, 'begin', handler));
    line.appendChild(this.createPlayerNumberCell(player, index, handler));
    line.appendChild(this.createPlayerNameCell(player, index, handler));
    line.appendChild(this.createPlayerPositionCell(player));
    line.appendChild(this.createPlayerCostCell(player));
    line.appendChild(this.createPlayerInteractCell(index, 'end', handler));
    return line;
  }

  updatePlayers(players, handler) {
    const table = document.getElementById('players');
    while (table.firstChild) {
      table.removeChild(table.firstChild);
    }
    players.forEach((player, index) => {
      table.appendChild(this.createPlayerLine(player, index, handler));
    });
  }
  updatePlayerCount(playerCount) {
    document.getElementById('team_player_count').innerText = playerCount;

  }
}