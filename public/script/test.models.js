/*
Copyright 2021 Bertrand Madet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

QUnit.module('Rules', () => {

  QUnit.test('constructor should handle JSON input modes', (assert) => {
    const input = JSON.parse(`{
      "modes": {"league":1000,"exhibition":1500},
      "rosters":{}
    }`);

    const rules = new Rules(input);
    assert.ok(rules);
    const entries = rules.modes.entries();
    assert.ok(rules.modes.has('league'));
    assert.propEqual(rules.modes.get('league'), { name: 'league', treasury: 1000 });
    assert.ok(rules.modes.has('exhibition'));
    assert.propEqual(rules.modes.get('exhibition'), { name: 'exhibition', treasury: 1500 });
  });

  QUnit.test('constructor should handle JSON input rosters', (assert) => {
    const input = JSON.parse(`{
      "modes": {},
      "rosters":{
        "human":{
          "reroll":50,
          "apothecary":true,
          "tier":1,
          "positions":{}
        },
        "halfling":{
          "reroll":60,
          "apothecary":true,
          "tier":3,
          "positions":{
            "halfling hopeful lineman":{
              "cost": 30,
              "minimum":0,
              "maximum":16,
              "color":"grey"
            },
            "halfling hefty":{
              "cost": 50,
              "minimum":0,
              "maximum":2,
              "color":"red"
            }
          }
        }
      }
    }`);

    const rules = new Rules(input);
    assert.ok(rules);
    assert.ok(rules.rosters.has('human'));
    assert.ok(rules.rosters.has('halfling'));
    assert.equal(rules.rosters.get('human').reroll, 50);
    assert.equal(rules.rosters.get('human').apothecary, true);
    assert.equal(rules.rosters.get('human').tier, 1);
    assert.ok(rules.rosters.get('human').positions);

    assert.ok(rules.rosters.get('halfling').positions);
    assert.ok(rules.rosters.get('halfling').positions.has('halfling hopeful lineman'));
    assert.propEqual(rules.rosters.get('halfling').positions.get('halfling hopeful lineman'), {
      name: 'halfling hopeful lineman',
      cost: 30,
      minimum: 0,
      maximum: 16,
      color: 'grey',
      agility: '-',
      armorValue: '-',
      movementAllowance: '-',
      passingAbility: '-',
      skills: [],
      strength: '-',
      traits: []
    });
    assert.ok(rules.rosters.get('halfling').positions.has('halfling hefty'));
    assert.propEqual(rules.rosters.get('halfling').positions.get('halfling hefty'), {
      name: 'halfling hefty',
      cost: 50,
      minimum: 0,
      maximum: 2,
      color: 'red',
      agility: '-',
      armorValue: '-',
      movementAllowance: '-',
      passingAbility: '-',
      skills: [],
      strength: '-',
      traits: []
    });
  });

  QUnit.test('modeNames should return list of mode names', (assert) => {
    const input = JSON.parse(`{
      "modes": {"league":1000,"exhibition":1500},
      "rosters":{}
    }`);

    const rules = new Rules(input);
    assert.deepEqual(rules.modeNames().sort(), ['exhibition', 'league']);
  });

  QUnit.test('rosterNames should return list of roster name', (assert) => {
    const input = JSON.parse(`{
      "modes": {},
      "rosters":{
        "human":{
          "reroll":50,
          "apothecary":true,
          "tier":1,
          "positions":{}
        },
        "halfling":{
          "reroll":60,
          "apothecary":true,
          "tier":3,
          "positions":{}
        }
      }
    }`);
    const rules = new Rules(input);
    assert.deepEqual(rules.rosterNames().sort(), ['halfling', 'human']);
  });
});

QUnit.module('Team', () => {

  QUnit.test('value should take into account rerolls', (assert) => {
    const team = new Team(new Roster('aRoster', {
      reroll: 1,
      tier: 1,
      apothecary: false
    }));
    assert.equal(team.rerollCount, 0);
    assert.equal(team.value(), 0);
    team.addReroll();
    assert.equal(team.value(), 1);
    team.removeReroll();
    assert.equal(team.value(), 0);
  });

  QUnit.test('addPlayer, removePlayer should work together', (assert) => {
    const team = new Team(new Roster('aRoster', {
      reroll: 1,
      apothecary: true,
      tier: 1,
      positions: {
        lineman: {
          cost: 50,
          minimum: 0,
          maximum: 16,
          color: "grey"
        },
        thrower: {
          cost: 80,
          minimum: 0,
          maximum: 2,
          color: "white"
        }
      }
    }));
    team.addPlayer('lineman');
    assert.equal(team.players.length, 1);
    assert.propEqual(team.players[0], {
      name: 'lineman 1',
      number: 1,
      position: new Position('lineman', {
        cost: 50,
        minimum: 0,
        maximum: 16,
        color: "grey"
      })
    });
    assert.equal(team.value(), 50);
    team.removePlayer(0);
    assert.equal(team.players.length, 0);
    assert.equal(team.value(), 0);
  });

  QUnit.test('value() should take into apothecary', (assert) => {
    const team = new Team(new Roster('aRoster', {
      reroll: 1,
      apothecary: true,
      tier: 1,
      positions: {}
    }));
    assert.equal(team.value(), 0);
    team.apothecary = true;
    assert.equal(team.value(), 50);
    team.apothecary = false;
    assert.equal(team.value(), 0);
  });

  QUnit.test('getAvailablePositions should display available positions', (assert) => {
    const team = new Team(new Roster('aRoster', {
      reroll: 1,
      apothecary: true,
      tier: 1,
      positions: {
        lineman: {
          cost: 50,
          minimum: 0,
          maximum: 1,
          color: "grey"
        }
      }
    }));
    assert.deepEqual(team.getAvailablePositions(), [{ position: 'lineman', available: true }]);
    team.addPlayer('lineman');
    assert.deepEqual(team.getAvailablePositions(), [{ position: 'lineman', available: false }]);

  });

  QUnit.test('getAvailablePositions should display available positions even with positionRestriction', (assert) => {
    const team = new Team(new Roster('aRoster', {
      reroll: 1,
      apothecary: true,
      tier: 1,
      positionRestriction: {
        "max": 1,
        "of": [
          "ogre",
          "treeman"
        ]
      },
      positions: {
        lineman: {
          cost: 50,
          minimum: 0,
          maximum: 1,
          color: "grey"
        },
        ogre: {
          cost: 50,
          minimum: 0,
          maximum: 1,
          color: "green"
        },
        treeman: {
          cost: 50,
          minimum: 0,
          maximum: 1,
          color: "green"
        }
      }
    }));
    function expectedAvailability(ogre, treeman) {
      return [
        { position: 'lineman', available: true },
        { position: 'ogre', available: ogre },
        { position: 'treeman', available: treeman },
      ];
    }
    assert.deepEqual(team.getAvailablePositions(), expectedAvailability(true, true));
    team.addPlayer('ogre');
    assert.deepEqual(team.getAvailablePositions(), expectedAvailability(false, false));

  });
});
