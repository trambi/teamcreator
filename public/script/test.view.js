/*
Copyright 2021 Bertrand Madet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

QUnit.module('createRosterTable', () => {

  QUnit.test('should display roster table', (assert) => {
    const roster = new Roster('aRoster', {
      reroll: 1,
      apothecary: true,
      tier: 1,
      positions: {
        lineman: {
          cost: 50,
          minimum: 0,
          maximum: 16,
          color: "grey",
          movementAllowance: 6,
          strength: 3,
          agility: "3+",
          passingAbility: "5+",
          armorValue: "9+",
          skills: [],
          traits: []
        },
        thrower: {
          cost: 80,
          minimum: 0,
          maximum: 2,
          color: "white",
          movementAllowance: 6,
          strength: 3,
          agility: "3+",
          passingAbility: "2+",
          armorValue: "9+",
          skills: ["Pass", "Sure Hands"],
          traits: []
        }
      }
    });
    const view = new View();
    const expected = `<tr>
    <td>0-16</td>
    <td>lineman</td>
    <td>50 k</td>
    <td>6</td>
    <td>3</td>
    <td>3+</td>
    <td>5+</td>
    <td>9+</td>
    <td></td>
</tr><tr>
    <td>0-2</td>
    <td>thrower</td>
    <td>80 k</td>
    <td>6</td>
    <td>3</td>
    <td>3+</td>
    <td>2+</td>
    <td>9+</td>
    <td>Pass, Sure Hands</td>
</tr>`;
    const result = view.createRosterTable(roster);
    assert.equal(result, expected);
  });

  QUnit.test('should display roster table with positionRestriction', (assert) => {
    const roster = new Roster('aRoster', {
      reroll: 1,
      apothecary: true,
      tier: 1,
      positionRestriction: {
        "max": 1,
        "of": [
          "thrower",
          "blitzer"
        ]
      },
      positions: {
        lineman: {
          cost: 50,
          minimum: 0,
          maximum: 16,
          color: "grey",
          movementAllowance: 6,
          strength: 3,
          agility: "3+",
          passingAbility: "5+",
          armorValue: "9+",
          skills: [],
          traits: []
        },
        thrower: {
          cost: 80,
          minimum: 0,
          maximum: 1,
          color: "white",
          movementAllowance: 6,
          strength: 3,
          agility: "3+",
          passingAbility: "2+",
          armorValue: "9+",
          skills: ["Pass", "Sure Hands"],
          traits: []
        },
        blitzer: {
          cost: 90,
          minimum: 0,
          maximum: 1,
          color: "red",
          movementAllowance: 7,
          strength: 3,
          agility: "3+",
          passingAbility: "4+",
          armorValue: "9+",
          skills: ["Block"],
          traits: []
        }
      }
    });
    const view = new View();
    const expected = `<tr>
    <td>0-16</td>
    <td>lineman</td>
    <td>50 k</td>
    <td>6</td>
    <td>3</td>
    <td>3+</td>
    <td>5+</td>
    <td>9+</td>
    <td></td>
</tr><tr><td colspan="9">A aRoster team may include up to 1 big guy from among the following</td>
</tr><tr>
    <td>0-1</td>
    <td>thrower</td>
    <td>80 k</td>
    <td>6</td>
    <td>3</td>
    <td>3+</td>
    <td>2+</td>
    <td>9+</td>
    <td>Pass, Sure Hands</td>
</tr><tr>
    <td>0-1</td>
    <td>blitzer</td>
    <td>90 k</td>
    <td>7</td>
    <td>3</td>
    <td>3+</td>
    <td>4+</td>
    <td>9+</td>
    <td>Block</td>
</tr>`;
    const result = view.createRosterTable(roster);
    assert.equal(result, expected);
  });
});
