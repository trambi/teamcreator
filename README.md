# TeamCreator

A vanilla javascript project to create our Bloodbowl version 2020 team. It is a dirty UI for now.

`public` directory consist of files to publish:

- `public/index.html` is the main html page;
- `public/test.html` is the test html page run with QUnit;
- `public/script` is javascript directory;
- `public/rules.json` is the settings in json;
- `public/style.css` is the css file.
